'''
Playing ataxx with 2 agents connected through TCP/IP sockets

Game transitions are delayed to enable human observation of the game

For the communication Standard for Ataxx see:
- the Message class in communication.py
- UML diagram provided in repository
'''

import argparse
import math
import os
import re
import socket
import time

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1' # disabling pygame hello message
import pygame as pg

from collections            import defaultdict
from ataxx.ataxxGame        import AtaxxGame, clone_neighborhood, jump_neighborhood
from ataxx.ataxxDisplay     import Display
from ataxx.ataxxEvalPlayers import RandomPlayer

from communication import decode, encode, Message

########################################################################
# Params
# on linux LAN network ip address can be read off using 'ifconfig' in a terminal
endpoint = ('localhost', 9000)

# timeout = 10 # seconds

# Should test if agent deals well with timeouts
# Setting to 0 times out all generated moves (except first 2 which are always unlimited time)
timeout = 0

FPS = 1               # 1s per move
selection_time = 0.5

########################################################################
Host = str
Port = int

def main(endpoint : tuple[Host, Port]) -> None:
    '''
    Wraps setting up the sizes and connecting to the agents
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("--size", type=int, required=True)

    args = parser.parse_args()
    assert args.size in [4,5,6]

    agent1, agent2 = connect_agents(endpoint, args.size)
    try:
        play(args.size, agent1, agent2)

    except Exception as e:
        print(f'Error: {e}')

    agent1.close()
    agent2.close()

    print('Press enter to exit')
    input() # a blocking call to stop here

########################################################################
# Sockets
def connect_agents(endpoint   : tuple[Host, Port],
                   board_size : int,
    ) -> tuple[socket.socket, socket.socket]:
    '''
    Connects to 2 agents and informs them of board size and which
    turn they have.
    '''
    print(f'Listening on {endpoint[0]}:{endpoint[1]}')
    listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # https://stackoverflow.com/questions/41208720/python-sockets-not-really-closing
    listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    listening_socket.bind(endpoint)
    listening_socket.listen()

    print('Waiting for connection of Agent 1..')
    agent1, addr = listening_socket.accept()
    print(f'Agent 1 connected from {addr}\n')

    print('Waiting for connection of Agent 2..')
    agent2, addr = listening_socket.accept()
    print(f'Agent 2 connected from {addr}\n')

    # inform agents of turn and board size
    agent1.sendall(
        Message.info
        .format(player='1', board_size=board_size)
        .encode()
    )

    agent2.sendall(
        Message.info
        .format(player='2', board_size=board_size)
        .encode()
    )

    listening_socket.close()

    return agent1, agent2

########################################################################
# Main playing functions
repetition_count = defaultdict(int) # Board → #visits

def play(board_size : int,
         agent1 : socket.socket,
         agent2 : socket.socket,
    ) -> None:
    '''
    Play the game with 2 agents connected by TCP/IP sockets
    '''
    clock = pg.time.Clock()    # something to control the rate of updates (by blocking)

    g = AtaxxGame(board_size)  # init your game
    d = Display(g)             # display class

    board = g.getInitBoard()
    d.display(board)

    # since players are [1,-1] we can use them to index into:
    agents = [None, agent1, agent2]
    turn = 1
    plays = 0
    while True:
        plays += 1

        # setting timeouts only on 2nd round
        # gives leeway for model load time
        if plays <= 2:  cur_timeout = math.inf
        else:           cur_timeout = timeout

        move, std_move = get_valid_move_from_player(g, board, agents, turn, cur_timeout)
        if move is None:
            # Only if connection was broken
            break

        print(f'{ turn if turn == 1 else 2 } → {std_move}')

        # transition with delay and beautification
        clock.tick(FPS)                             # assure at least 1/FPS time has elapsed
        pretty_display(board, d, std_move)          # paint selected piece and it's range
        time.sleep(selection_time)                  # show the selected for some time
        board, turn = g.getNextState(board, turn, move)

        # switch turn
        # done

        # display
        d.display(board)                               # paint transitioned board

        # game over? just return, should not kill process so that condition can be graphically inspected
        # if g.getGameEnded(board, turn) != 0:
        if (result := is_game_over_wrapper(g, board, turn)) != 0:
            # inform players the game is over
            msg = Message.end.encode()
            agents[ turn].sendall(msg)
            agents[-turn].sendall(msg)

            # If game is over due to Threefold repetition rule
            # Scores are irrelevant
            if result == 42:
                print('\n\n'
                      'END 0 _ _ (Repeated boards)'
                      '\n\n'
                )
                return

            # outside the loop is the traditional ending
            break

    # Display results
    score1, score2 = count_pieces(board)
    if   score1 > score2: winner = 1
    elif score1 < score2: winner = 2
    else:                 winner = 0

    print('\n\n'
          f'END {winner} {score1} {score2}'
          '\n\n'
    )

def get_valid_move_from_player(g : AtaxxGame,
                               board,
                               agents : list[socket.socket],
                               turn   : int,
                               cur_timeout : float,
                               ) -> tuple[int | None, str]:
    '''
    Gives 3 chances for the agent to provide a valid move in a
    decent amount of time.

    Exhausting those 3 chances the server chooses a random move and
    informs the current player to execute it. (In go this should be a
    Pass instead of random)
    '''

    # Calculate valid moves
    valids = g.getValidMoves(board, turn)

    # Get valid move from current player
    # 3 chances for valid move, 3rd failure it's a:
    # - Ataxx : random move (Pass is only valid when nothing else is)
    # - Go    : Pass move
    for k in range(1,4): # k in [1,2,3]
        s = time.time()

        std_move : str = agents[turn].recv(1024).decode()

        if time.time() - s >= cur_timeout:
            print(f'{ turn if turn == 1 else 2 } → time out :: forcing: ', end='')

            return gen_rand_move_and_notify(g, board, turn, agents, Message.timeout_rand)

        if std_move == '':
            print(f'Agent { turn if turn == 1 else 2  } left')  # current agent broke the connection
            agents[-turn].sendall(Message.endl.encode())        # inform the other player to end
            return None, ''

        # converting to internal move representation
        move = decode(std_move, g) # an impossible (not in action space) move will be None

        # validate it
        if move is not None and valids[move] == 1:
            agents[ turn].sendall(Message.valid.encode())  # inform current player move is valid
            agents[-turn].sendall(std_move.encode())       # inform the other player of the move

            return move, std_move

        elif (move is None or valids[move] == 0) and k < 3:
            # an invalid move, giving the agent another chance
            print(f'{ turn if turn == 1 else 2 } → INVALID : {std_move}')
            agents[turn].sendall(Message.invalid.encode())

        elif k == 3:
            # 3rd invalid move, generate a random move
            print(f'3rd invalid move by player { turn if turn == 1 else 2 } :: forcing: ', end='')

            return gen_rand_move_and_notify(g, board, turn, agents, Message.invalid_rand)

def gen_rand_move_and_notify(g : AtaxxGame,
                             board,
                             turn : int,
                             agents : list[socket.socket],
                             template : str,
    ) -> tuple[int, str]:
    '''
    Generates a random and informs both players.
    The offending player receives either the INVALID {move} or TIMEOUT {move}
    '''

    # Ataxx : Generate a random move
    rand_move = RandomPlayer(g).play(g.getCanonicalForm(board, turn))
    std_rand_move = encode(rand_move, g, board, turn)

    agents[ turn].sendall(
        template
        .format(move=std_rand_move)
        .encode()
    )
    agents[-turn].sendall(
        std_rand_move
        .encode()
    )

    return rand_move, std_rand_move

def is_game_over_wrapper(g : AtaxxGame, board, turn) -> float:
    '''
    Main interface does not (and cannot) implement the
    Threefold repetition rule (https://en.wikipedia.org/wiki/Threefold_repetition).
    So that is done here:
    - on 3rd repeated board the game is declared a draw
    '''
    b = board.tobytes()
    repetition_count[b] += 1

    if repetition_count[b] >= 3:
        return 42
    else:
        return g.getGameEnded(board, turn)

########################################################################
# Some helpers
def pretty_display(board, d : Display, std_move):
    '''
    Extra beautification:
    - paints selected piece for move
    - and it's range
    '''
    matched = re.search('MOVE \D*(\d)\D*(\d)\D*(\d)\D*(\d)', std_move)

    assert matched is not None

    row = int(matched.group(1))
    col = int(matched.group(2))

    start_pos = (row, col)

    if d.game.turn == 1: color = 'red'
    else:                color = 'blue'

    # paint selection
    d.display_surf.blit( d.assets[f'{color}_selected'], (start_pos[1]*d.cell_size, start_pos[0]*d.cell_size) )

    # paint possible destinations
    for pos in d.game.iter_neighborhood(start_pos, clone_neighborhood):

       if board[pos] == 0:
           d.display_surf.blit( d.assets[f'{color}_clone'], (pos[1]*d.cell_size, pos[0]*d.cell_size) )

    for pos in d.game.iter_neighborhood(start_pos, jump_neighborhood):
       if board[pos] == 0:
           d.display_surf.blit( d.assets[f'{color}_jump'], (pos[1]*d.cell_size, pos[0]*d.cell_size) )


    pg.display.flip()

# AtaxxGame does not implement this function, only fused versions of them
# so we do it here
def count_pieces(board) -> tuple[int, int]:
    '''
    Basic accounting of number of pieces
    '''
    p1 = 0
    p2 = 0

    for row in range(board.shape[0]):
        for col in range(board.shape[1]):

            if   board[row, col] ==  1: p1 += 1
            elif board[row, col] == -1: p2 += 1

    return p1, p2


if __name__ == '__main__':
    main(endpoint)
