'''
Includes:
- Communication standard
- Coder Decoder from Standard to implementation specific action representation
'''
import re

from ataxx.ataxxGame import AtaxxGame, clone_neighborhood

# The standard
class Message:
    info    = 'AG{player} A{board_size}x{board_size}'    # inform agent of turn and board size
    endl    = 'END Left'                                 # one agent closed connection, inform other agent
    valid   = 'VALID'                                    # inform agent move is valid, can proceed
    invalid = 'INVALID'                                  # inform agent move is invalid, try again
    invalid_rand = 'INVALID {move}'                      # inform agent no more chances, server made random choice, makes use of move = Message.move interpolated or Message.Pass
    timeout_rand = 'TIMEOUT {move}'

    move      = 'MOVE {r0}, {c0}, {r1}, {c1}'
    pass_move = 'PASS'
    end       = 'END'

    # For GO
    # info    = 'AG{player} G{board_size}x{board_size}'  # inform agent of turn and board size
    # invalid_pass = 'INVALID PASS'                      # inform agent no more chances, she must PASS
    # move = 'MOVE {r}, {c}'

    @staticmethod
    def parse_info(info : str) -> tuple[int, int]:
        '''
        Parses the actual Message.info received
        Returns: turn, board_size
        '''
        matched = re.search('AG(.*) A(.*)x(.*)', info)
        assert matched is not None

        turn, board_size, _ = matched.groups()
        turn , board_size   = int(turn), int(board_size)

        return turn, board_size

    @staticmethod
    def parse_move(msg : str) -> tuple[int, int, int, int]:
        '''
        Parse a MOVE message
        '''
        matched = re.search('MOVE \D*(\d)\D*(\d)\D*(\d)\D*(\d)', msg)
        if matched is None:
            raise Exception("Couldn't regex match")

        r0, c0, r1, c1 = matched.groups()
        r0, c0, r1, c1 = int(r0), int(c0), int(r1), int(c1)

        return r0, c0, r1, c1

########################################################################
# Coder Decoder pair
def encode(l_action, game : AtaxxGame, board, turn) -> str:
    '''
    Internal to Standard Representation
    '''
    action = game.decode_action(int(l_action)) # internal linear action to Clone | Jump | Pass action

    match action:
        # Clone
        case tuple((row_dest, col_dest)) if isinstance(row_dest, int) and isinstance(col_dest, int):
            # we have destination but we need source to present as standard
            # iterate the board neighborhood any piece of current player works
            for row_origin, col_origin in game.iter_neighborhood((row_dest, col_dest), clone_neighborhood):
                if board[row_origin, col_origin] == turn:

                    return Message.move.format(** {
                        'r0' : row_origin,
                        'c0' : col_origin,
                        'r1' : row_dest,
                        'c1' : col_dest,
                    })


            raise Exception('No player piece found')

        # Jump
        case tuple((start_pos, offset)) if isinstance(start_pos, tuple) and isinstance(offset, tuple):
            row_origin = start_pos[0]
            col_origin = start_pos[1]

            row_dest = row_origin + offset[0]
            col_dest = col_origin + offset[1]

            return Message.move.format(** {
                'r0' : row_origin,
                'c0' : col_origin,
                'r1' : row_dest,
                'c1' : col_dest,
            })

        # Pass
        case None:
            return Message.pass_move

        case _:
            raise Exception

def decode(str, game : AtaxxGame): # -> Action | None
    '''
    Standard to Internal representation
    '''
    # print(f'Going to decode: {str}')

    if str == Message.pass_move:
        return game.encode_action(None)

    row_origin, col_origin, row_dest, col_dest = Message.parse_move(str)

    offset_row = row_dest - row_origin
    offset_col = col_dest - col_origin

    # A clone
    if -1 <= offset_row <= 1 and -1 <= offset_col <= 1:
        return game.encode_action( (row_dest, col_dest) )

    # A jump
    elif -2 <= offset_row <= 2 and -2 <= offset_col <= 2:
        action = ((row_origin, col_origin), (offset_row, offset_col))
        # print(f'Decoded to Jump action: {action}')
        return game.encode_action(action)

    # Absolutely Invalid
    else:
        return None
