'''
Random and Greedy client agent
'''

import socket
import sys
sys.path.append('../') # load modules like on top level directory


from ataxx.ataxxGame        import AtaxxGame
from ataxx.ataxxDisplay     import Display
from ataxx.ataxxEvalPlayers import RandomPlayer, GreedyPlayer

from communication import encode, decode, Message
########################################################################
# Params
endpoint = ('localhost', 9000)

########################################################################
Host = str
Port = int

def main(endpoint : tuple[Host, Port]) -> None:

    server, turn, board_size = connect_server(endpoint)
    try:
        play(board_size, turn, server)

    except Exception as e:
        print(f'Error: {e}')

    server.close()

    print('Press enter to exit')
    input() # a blocking call to stop here


########################################################################
# Sockets
def connect_server(endpoint : tuple[Host, Port]) -> tuple[socket.socket, int, int]:
    '''
    Connects to server and obtains from it turn and board_size information
    '''

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print(f'Connecting to {endpoint[0]}:{endpoint[1]}..')
    s.connect(endpoint)
    print('Connected')

    info = s.recv(1024).decode()
    turn, board_size = Message.parse_info(info)

    if turn == 2: turn = -1

    print(f'Taking the role of player={turn} in a {board_size}x{board_size} board\n')

    return s, turn, board_size

########################################################################
# Main playing function
def play(board_size : int,
         player : int,
         server : socket.socket,
    ) -> None:

    # Since server launches a window and this one does too,
    # this allows to know which is which.
    print('Press enter to start game window and continue...')
    input()

    g = AtaxxGame(board_size)  # init your game
    d = Display(g)

    board = g.getInitBoard()
    d.display(board)

    ########################################################################
    # Choose between random agent and Greedy agent
    # agent  = RandomPlayer(g).play
    agent  = GreedyPlayer(g).play
    ########################################################################

    turn  = 1
    while True:
        if player == turn:                                   # player has the turn
            move, std_move = server_validated_move(g, board, agent, player, server)

        else:                                                # other player has turn
            std_move = server.recv(1024).decode()            # receive move
            if std_move == '':
                print('Server left')
                return
            elif std_move == Message.endl:
                print('Other player left, leaving too..')
                return

            move = decode(std_move, g)                       # decode move from standard
            print(f"{ turn if turn == 1 else 2 } (their) move → {std_move}")             # log to stdout

        # check valid, invalid exits
        valids = g.getValidMoves(board, turn)

        if valids[move] == 0:  # invalid move
            print('Server poorly checked validity')
            return

        # transition
        board, turn = g.getNextState(board, turn, move)  # type:ignore
        d.display(board)

        # switch turn (if transition functions returns turn, one can use that)
        # turn *= -1

        # game over? just return
        if (result := g.getGameEnded(board, player)) != 0:
            print(f'Result: {result}')
            return


def server_validated_move(g : AtaxxGame,
                          board,
                          agent,
                          player : int,
                          server : socket.socket,
    ) -> tuple[int, str]:
    # let server tell us whether move was accepted
    while True:
        move = agent(g.getCanonicalForm(board, player))  # generate a move

        std_move = encode(move, g, board, player)        # encode move to standard
        server.sendall(std_move.encode())                # send move

        print(f'{ player if player == 1 else 2 } (our  ) move → {std_move} ', end='')        # log to stdout

        # Did server validate our move?
        resp = server.recv(1024).decode()
        # Valid
        if resp == Message.valid:
            print(Message.valid)
            return move, std_move

        # 3rd Invalid or timeout, override
        elif 'INVALID' in resp or 'TIMEOUT' in resp:
            # Remove 'INVALID '/'TIMEOUT ' component and parse it (they are the same length)
            inv_len = len(Message.invalid)
            move_part = resp[inv_len + 1 : ]

            if move_part == Message.pass_move:
                std_move = Message.pass_move
                move     = decode(std_move, g)

            else:
                # A proper move
                std_move = move_part
                move     = decode(std_move, g)

            assert move is not None, f'Server override could not be decoded {std_move}'

            print(f'Server override → {std_move}')
            return move, std_move

        else:
            # A Message.invalid
            # So try again
            # could change parameters like MCTS simulations?
            ...

########################################################################
if __name__ == '__main__':
    main(endpoint)
