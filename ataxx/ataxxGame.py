'''Ataxx game implementation

Representation:
- Players : {1, -1}
- Board : n × n → players ∪ {0, 8} (empty + walls)
  so encoded as:
  - np.ndarray,
    shape=(n, n),
    dtype=np.int8 (smallest possible data type)

- Action = Clone | Jump | Pass
  - Clone = (row, col)                  we only need to know the destination
  - Jump  : ((row, col), (drow, dcol))  requires a start and an offset ∈ jump_neighborhood
  - Pass  : None
  |Action| = n² + n²*16 + 1             not all valid (the jumps to outside the board)

  The Action Space requires a linear encoding representation since part of the
  output of the neural network will be a policy vector:
  - π: LinearAction → Prob
  So  LinearAction = {0,...,n²-1} ∪ {n²,..., 17n²-1} ∪ {17n²}
                       Clone            Jump            Pass

  - Clone: (row, col)                ↦ row*n + col                                       i.e. linear position in row major order
  - Jump:  ((row,col), (drow, dcol)) ↦ n² + (row*n+col)*16 + jump_2_index(drow,dcol)     where  jump_2_index : jump_neighborhood → {0,..,15}
  - Pass:  None                      ↦ 17n²

- Reward: {1, -1, 0.01} (victory, loss, draw)
'''
import numpy as np

from itertools import chain
########################################################################
## Weak typing
Player        = int          # Player = {1, -1}
Board         = np.ndarray   # shape=(n, n), pseudo-dtype = Player ∪ {0,8}
GameCondition = float        # {0, 1, -1, 0.01}
                             # 0 : not over, 1 : current player won, -1: current player lost, 0.01: draw

Position = tuple[int,int]
Vector   = Position
Clone    = Position
Jump     = tuple[Position, Vector]
Pass     = None
Action   = Clone | Jump | Pass
LinearAction = int

Pi = np.ndarray              # Policy vector : LinearAction → [0,1] where ∑pi = 1
Example = tuple[Board, Pi]   # Example type return by getSymmetries, Coach.py then adds the reward (outcome)

########################################################################
## Neighborhood maps
# clone_neighborhood : Index → Vector
clone_neighborhood = [
    (-1,-1), (-1,0), (-1,1),
    ( 0,-1),         ( 0,1),
    ( 1,-1), ( 1,0), ( 1,1),
]

# jump_neighborhood : Index → Vector
jump_neighborhood = [
    (-2,-2), (-2,-1), (-2,0), (-2, 1), (-2,2),
    (-1,-2),                           (-1,2),
    ( 0,-2),                           ( 0,2),
    ( 1,-2),                           ( 1,2),
    ( 2,-2), ( 2,-1), ( 2,0), ( 2, 1), ( 2,2),
]

# We will require the inverse map many times when calculating valid moves.
# Instead of doing linear search each time we make use of an associative array
# Speed difference is noticeable when compounded over many many usages:
# In ipython:
# Time for first entry of list:
# %timeit jump_neighborhood.index((-2,-2)) → 79.2 ns ± 1.59 ns per loop (mean ± std. dev. of 7 runs, 10,000,000 loops each)
# %timeit jump_2_index[(-2,-2)]            → 64.9 ns ± 0.861 ns per loop (mean ± std. dev. of 7 runs, 10,000,000 loops each)
# Time for last entry of list:
# %timeit jump_neighborhood.index((2,2))   → 399 ns ± 3.11 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)
# %timeit jump_2_index[(2,2)]              → 67 ns ± 0.604 ns per loop (mean ± std. dev. of 7 runs, 10,000,000 loops each)

# jump_2_index : jump_neighborhood → {0,..,15}
jump_2_index = { offset : jump_neighborhood.index(offset)
                 for offset in jump_neighborhood }

class AtaxxGame:
    '''
    Class works mostly as a name space for functions.
    '''
    n            : int              # board size
    n_squared    : int              # caching it
    n_squared_17 : int
    num_jumps    : int

    board_shape  : tuple[int, int]
    jump_shape   : tuple[int, int, int]

    turn : Player # only used for Display for human play

    # Caching the transformation to Jump component of policy vector
    # Theses are rotated indices
    rot90_ax0 : np.ndarray
    rot90_ax1 : np.ndarray
    rot90_ax2 : np.ndarray

    flip_ax0 : np.ndarray
    flip_ax1 : np.ndarray
    flip_ax2 : np.ndarray

    def __init__(self, n : int = 7) -> None:
        self.n            = n
        self.n_squared    = n*n
        self.n_squared_17 = 17 * self.n_squared
        self.num_jumps    = len(jump_neighborhood)

        self.board_shape = (self.n, self.n)
        self.jump_shape  = (self.n, self.n, self.num_jumps)

        self.turn = 1

        ############ Preparing transformation indices
        self._rot90_axes()


    def _rot90_axes(self) -> None:
        '''
        Construct rotated indices to obtain jump component of policy vector rotation
        '''
        # Board component rotation and flip
        ax0, ax1, _ = np.indices(self.jump_shape)
        self.rot90_ax0 = np.rot90(ax0) # counterclockwise
        self.rot90_ax1 = np.rot90(ax1)

        self.flip_ax0 = np.flip(ax0, axis=1)
        self.flip_ax1 = np.flip(ax1, axis=1)

        # Jump component rotation and flip
        indices = np.array(
            [
                [ 0, 1, 2, 3, 4],
                [ 5,-1,-1,-1, 6],
                [ 7,-1,-1,-1, 8],
                [ 9,-1,-1,-1,10],
                [11,12,13,14,15],
            ],
            dtype=np.int8
        )

        def extract_indices(arr) -> list[int]:
            l = []

            for i in range(arr.shape[0]):
                for j in range(arr.shape[1]):

                    if arr[i,j] != -1:
                        l.append(arr[i,j])

            return l

        rot90_indices = np.rot90(indices, 1, axes=(0,1))
        flip_indices  = np.flip(indices, axis=1)

        a = extract_indices(rot90_indices)
        b = extract_indices(flip_indices)

        assert np.array_equal(
            a, np.array([4, 6, 8, 10, 15, 3, 14, 2, 13, 1, 12, 0, 5, 7, 9, 11])
        )

        assert np.array_equal(
            b, np.array([4, 3, 2, 1, 0, 6, 5, 8, 7, 10, 9, 15, 14, 13, 12, 11])
        )

        self.rot90_ax2 = np.broadcast_to(a, shape=self.jump_shape)
        self.flip_ax2  = np.broadcast_to(b, shape=self.jump_shape)

    def getInitBoard(self) -> Board:
        b = np.zeros(shape=self.board_shape, dtype=np.int8)
        b[0, 0] = b[-1,-1] =  1
        b[0,-1] = b[-1, 0] = -1

        # Hard coded walls works just fine for it's purpose
        if self.n == 5:
            b[0,2] = 8
            b[1,1] = b[1,3] = 8
            b[3,1] = b[3,3] = 8
            b[4,2] = 8

        return b

    def getBoardSize(self) -> tuple[int, int]:
        return self.board_shape

    def getActionSize(self) -> int:
        '''
        |Action| = 17*n² + 1
        '''
        return self.n_squared_17 + 1

    def getNextState(self,
                     board    : Board,
                     player   : Player,
                     l_action : LinearAction
                     ) -> tuple[Board, Player]:
        '''
        Returns a copy of the board: mandatory for MCTS.
        '''
        board = np.copy(board)

        # When output comes from NN it comes out np.in64 which will
        # not match the pattern later, hence explicit conversion
        action : Action \
            = self.decode_action(int(l_action))

        self.turn = -player

        match action:
            # Pass
            case None:
                return board, -player

            # Clone
            case tuple((row, col)) if isinstance(row, int) and isinstance(col, int):
                board[row, col] = player

                self.conquer_adjacent(board, (row,col), player)  # board is mutated

                return board, -player

            # Jump
            case tuple((start_pos, offset)) if isinstance(start_pos, tuple) and isinstance(offset, tuple):
                dest = (start_pos[0] + offset[0],
                        start_pos[1] + offset[1])

                board[start_pos] = 0
                board[dest]      = player

                self.conquer_adjacent(board, dest, player)  # board is mutated

                return board, -player

            case _:
                raise Exception(f'l_action={l_action} : {type(l_action)} ;; action={action} : {type(action)}')

    def getValidMoves(self, board : Board, player : Player) -> np.ndarray:
        '''
        returns a vector : |Action| → {0,1}
        ie. shape=(|Action|, )

        valid actions are 1
        '''
        shape = (self.getActionSize(), )
        valid = np.zeros(shape=shape, dtype=np.uint8)

        # Views on valid allows not to create these arrays and then copying in order to concatenate
        clone = valid[0 : self.n_squared].reshape(self.board_shape)
        jump  = valid[self.n_squared: -1].reshape(self.jump_shape)  # exclude pass

        # iterate the board
        # s_* :: start
        # d_* :: destination
        found_valid = False
        for s_row in range(self.n):
            for s_col in range(self.n):

                if board[s_row, s_col] == player:
                    # clone neighborhood
                    for dest in self.iter_neighborhood((s_row,s_col), clone_neighborhood):
                        if board[dest] == 0:
                            clone[dest] = 1
                            found_valid = True

                    # jump neighborhood
                    for dest in self.iter_neighborhood((s_row, s_col), jump_neighborhood):
                        if board[dest] == 0:
                            offset = (dest[0] - s_row, dest[1] - s_col)
                            index = jump_2_index[offset]

                            jump[s_row, s_col, index] = 1
                            found_valid = True

        if not found_valid:
            valid[-1] = 1 # No move found so pass is valid

        return valid

    def getGameEnded(self, board, player) -> GameCondition:
        '''
        Game ends
        ⇔  both players have no moves left except passing   (contemplates boards with all cells reachable and those without)
          ∨ one of them lost all pieces

        I.e.
        Game does NOT end
        ⇔ one of them has actual moves
         ∧ both have pieces

        ⇔ one has actual moves
          ∧ other has pieces     (this is because having a move ⇒ having a piece)

        Returns:
         0 if game not over (current player might still have no move besides passing)
         1 if current player won
        -1 if current player lost
         0.01 if draw

        (win condition = most pieces)
        '''
        assert board.shape == (self.n, self.n)

        cur_p = 0
        oth_p = 0

        cur_has_move = False
        oth_has_move = False

        for row in range(self.n):
            for col in range(self.n):

                if   board[row,col] ==  player: cur_p += 1
                elif board[row,col] == -player: oth_p += 1

                elif board[row,col] ==  0: # checking if move available
                    for pos in self.iter_neighborhood((row, col),
                                                      chain(clone_neighborhood, jump_neighborhood)):

                        if board[pos] == player:
                            cur_has_move = True  # current player has actual move

                            if oth_p > 0:
                                # Early stopping:  current has actual moves ∧ other has pieces ⇒ Game not over
                                # print(f'Early stopping: current')
                                return 0

                        elif board[pos] == -player:
                            oth_has_move = True  # other player has actual move

                            if cur_p > 0:
                                # Early stopping:  other has actual moves ∧ current has pieces ⇒ Game not over
                                # print(f'Early stopping: other')
                                return 0

        assert cur_p != 0 or oth_p != 0, 'Empty board is not a valid game'

        # one has actual moves ∧ other has pieces ⇒ Game not over
        if cur_has_move and oth_p > 0:    return 0
        if cur_p > 0    and oth_has_move: return 0

        # So either none have an actual move or some player has 0 pieces.
        # Game is over
        if cur_p > oth_p : return  1     # current player won
        if cur_p < oth_p : return -1     # other   player won
        else:              return  0.01  # draw

    def getCanonicalForm(self, board, player):
        '''
        More like canonical pieces not canonical board
        Gives a board representation where the player with the turn
        has his pieces labeled with a '1'.

        She can still distinguish (if so desired) if truly a '1' or a '-1'.
        Eg. in starting position:
        - if you have a piece in bottom left you are truly a '-1'
        - if you have a piece in bottom right you are truly a '1'
        '''
        if player == 1:  return    board
        else:            return -1*board

    def reverseCanonicalForm(self, board):
        '''
        Used exclusively for ataxxDisplay.Display

        Arena calls display only with the canonical board
        but presentation should be of the true board.
        self.turn is kept to allow to revert the canonizing.
        '''
        player = self.turn
        return self.getCanonicalForm(board, player)

    def getSymmetries(self, board : Board, pi : Pi) -> list[Example]:
        '''
        D4 dihedral group symmetries: 4 rotations + 4 reflections
        G = {a := counter_clockwise rotation, b := vertical symmetry flip}
        <G> = D4 i.e. generates D4

        We calculate algorithmically:
        - a⁰ == id, a,  a²,  a³
        - a⁰b == b, ab, a²b, a³b

        https://en.wikipedia.org/wiki/Dihedral_group
        https://en.wikipedia.org/wiki/Examples_of_groups#The_symmetry_group_of_a_square:_dihedral_group_of_order_8 (note their 'a' is clockwise rotation i.e. our a^{-1} )
        '''
        examples : list[Example] = []
        pi = np.asarray(pi)

        # init variable name for loop
        # all board rotations will be views of the same underlying memory
        rot_board = board

        # Obtain View for Clone and Jump tensors
        rot_clone_pi = pi[0 : self.n_squared].reshape(self.board_shape)
        rot_jump_pi  = pi[self.n_squared: -1].reshape(self.jump_shape)  # exclude pass

        for k in range(4):
            # first iteration is NO rotation
            if k != 0:
                # rotation 90º counter clockwise
                rot_board    = np.rot90(rot_board)                                         # a view
                rot_clone_pi = np.rot90(rot_clone_pi)                                      # a view
                rot_jump_pi  = rot_jump_pi[self.rot90_ax0, self.rot90_ax1, self.rot90_ax2] # full copy (found not other way to do this)

                assert not rot_board.flags['OWNDATA']
                assert not rot_clone_pi.flags['OWNDATA']
                assert rot_jump_pi.flags['OWNDATA']

            flip_board    = np.flip(rot_board,    axis=1)                             # a view
            flip_clone_pi = np.flip(rot_clone_pi, axis=1)                             # a view
            flip_jump_pi  = rot_jump_pi[self.flip_ax0, self.flip_ax1, self.flip_ax2]  # full copy (found not other way to do this)

            assert not flip_board.flags['OWNDATA']
            assert not flip_clone_pi.flags['OWNDATA']
            assert flip_jump_pi.flags['OWNDATA']

            # Instead of ravel use reshape, ravel might copy since it tries to make it contiguous
            # but concatenate needs to build a new one anyway
            # rot_pi = np.concatenate((rot_clone_pi.ravel(),
            #                          rot_jump_pi.ravel(),
            #                          [pi[-1]], # add back the Pass probability
            #                          ))

            # flip_pi = np.concatenate((flip_clone_pi.ravel(),
            #                           flip_jump_pi.ravel(),
            #                           [pi[-1]], # add back the Pass probability
            #                           ))

            rot_pi = np.concatenate((rot_clone_pi.reshape((rot_clone_pi.size, )),
                                       rot_jump_pi.reshape((rot_jump_pi.size, )),
                                       [pi[-1]], # add back the Pass probability
                                    ))

            flip_pi = np.concatenate((flip_clone_pi.reshape((flip_clone_pi.size, )),
                                      flip_jump_pi.reshape((flip_jump_pi.size, )),
                                      [pi[-1]], # add back the Pass probability
                                      ))

            examples.append((rot_board,  rot_pi))
            examples.append((flip_board, flip_pi))

        return examples
        ########################################################################

    def stringRepresentation(self, board) -> str:
        # return ''.join(str(x) for row in board for x in row)
        return board.tobytes()

    def decode_action(self, l_action : LinearAction, ignore_bounds=False) -> Action:
        assert 0 <= l_action <= self.n_squared_17

        if l_action == self.n_squared_17:
            return Pass

        if l_action < self.n_squared:
            # A Clone action
            row = l_action // self.n
            col = l_action %  self.n
            return (row, col)

        # A Jump action
        l_action -= self.n_squared # offset it back, so l_action now ∈ {0, ..., 16n² -1}

        pos_enc   = l_action // self.num_jumps
        off_index = l_action %  self.num_jumps

        start_pos = (pos_enc // self.n,
                     pos_enc %  self.n)

        offset = jump_neighborhood[off_index]

        # ignore bounds is usefull when proving bijection
        # between decode_action and encode_action
        if not ignore_bounds:
            adj_row = start_pos[0] + offset[0]
            adj_col = start_pos[1] + offset[1]
            assert 0 <= adj_row < self.n, f'start_pos={start_pos}, offset={offset}'
            assert 0 <= adj_col < self.n

        return (start_pos, offset)

    def encode_action(self, action : Action) -> LinearAction:
        match action:
            # Clone
            case tuple((row, col)) if isinstance(row, int) and isinstance(col, int):
                assert 0 <= row < self.n
                assert 0 <= col < self.n

                return row * self.n + col

            # Jump
            case tuple((start_pos, offset)) if isinstance(start_pos, tuple) and isinstance(offset, tuple):
                row = start_pos[0]
                col = start_pos[1]

                assert 0 <= row < self.n
                assert 0 <= col < self.n

                pos_enc   = row * self.n + col
                off_index = jump_neighborhood.index(offset)

                return (pos_enc * self.num_jumps + off_index) + self.n_squared

            # Pass
            case None:
                return self.n_squared_17

            case _:
                raise Exception


    from collections.abc import Iterator, Iterable

    def iter_neighborhood(self,
                          pos : Position,
                          neigh: Iterable[Vector]) -> Iterator[Position]:
        '''
        returns an iterator that avoids out of board positions
        '''
        row = pos[0]
        col = pos[1]

        def f():
            for drow, dcol in neigh:
                adj_row = row + drow
                adj_col = col + dcol

                if (0 <= adj_row < self.n and
                    0 <= adj_col < self.n):

                    yield (adj_row, adj_col)

        return f()

    def conquer_adjacent(self,
                         board : Board,
                         pos   : Position,
                         player : Player) -> None:
        '''
        Mutates the board, conquering adjacent cells
        '''
        for (adj_row, adj_col) in self.iter_neighborhood(pos, clone_neighborhood):
            if board[adj_row, adj_col] == -player:
                board[adj_row, adj_col] = player  # conquered
