import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1' # disabling pygame hello message
import pygame as pg
import time

from .ataxxGame import AtaxxGame, Board, LinearAction, clone_neighborhood, jump_neighborhood, Position

main_dir : str = os.path.split(os.path.abspath(__file__))[0]

class Display:
    game       : AtaxxGame
    cell_size  : int
    assets     : dict[str, pg.Surface]
    background_surf : pg.Surface
    display_surf    : pg.Surface
    FPS        : int = 60

    def __init__(self, game : AtaxxGame) -> None:
        '''
        Computes biggest cell_size for the current screen.
        Initializes the screen and the creates the background surface
        '''
        self.game = game

        board : Board = game.getInitBoard()
        rows  : int = board.shape[0]
        cols  : int = board.shape[1]

        import platform
        if platform.system() == 'Linux':
            os.environ['SDL_VIDEODRIVER'] = 'x11' # wayland display sometimes bugs

        pg.init() # Initialize pygame
        # print(pg.display.get_driver())

        ########################
        # Use current monitor's resolution to compute the most appropriate cell size
        max_screen_width:  int = pg.display.Info().current_w
        max_screen_height: int = pg.display.Info().current_h

        max_cell_width : int = max_screen_width  // cols
        max_cell_height: int = max_screen_height // rows

        cell_size : int = min(max_cell_width,
                              max_cell_height)

        # let's make it so more than 1 board fits on screen
        cell_size = int(cell_size / 1.5)
        self.cell_size = cell_size

        window_width : int = cols * cell_size
        window_height: int = rows * cell_size

        # Display surface
        self.display_surf : pg.Surface \
            = pg.display.set_mode((window_width, window_height), flags=pg.RESIZABLE)

        # load assests
        # selected requires alpha transparency since it's overlaid on piece
        self.assets = {
            'empty'      : load_image('pixel_board_empty.png',           cell_size),
            'blue'       : load_image('pixel_board_blue.png',            cell_size),
            'red'        : load_image('pixel_board_red.png',             cell_size),
            'wall'       : load_image('pixel_board_wall.png',            cell_size),
            'blue_clone' : load_image('pixel_board_blue_dest_clone.png', cell_size),
            'blue_jump'  : load_image('pixel_board_blue_dest_jump.png',  cell_size),
            'red_clone'  : load_image('pixel_board_red_dest_clone.png',  cell_size),
            'red_jump'   : load_image('pixel_board_red_dest_jump.png',   cell_size),
            'blue_selected': load_image('pixel_board_blue_selected.png', cell_size),
            'red_selected' : load_image('pixel_board_red_selected.png',  cell_size),

        }

        self.background_surf : pg.Surface \
            = self.build_background_surface(board, window_width, window_height, cell_size)

    def build_background_surface(self,
                                 board : Board,
                                 background_width:  int,
                                 background_height: int,
                                 cell_size:         int,
                                 ) -> pg.Surface:

        background_surf : pg.Surface \
            = pg.Surface(size=(background_width, background_height))

        for row in range(board.shape[0]):
            for col in range(board.shape[1]):
                if board[row, col] == 8:
                    paint_with = 'wall'
                else:
                    paint_with = 'empty'

                background_surf.blit( self.assets[paint_with], (col*self.cell_size, row*self.cell_size) )

        return background_surf

    def display(self, board : Board) -> None:
        # Paint the background
        self.display_surf.blit(self.background_surf, dest=(0,0))

        for row in range(board.shape[0]):
            for col in range(board.shape[1]):

                if board[row, col] == 1: # p1 is red piece
                    self.display_surf.blit( self.assets['red'], (col*self.cell_size, row*self.cell_size) )


                elif board[row, col] == -1: # p1 is red piece
                    self.display_surf.blit( self.assets['blue'], (col*self.cell_size, row*self.cell_size) )

        pg.display.flip()

    def human_play(self, canonical_board : Board) -> LinearAction:
        # receives the canonical board... reverse it back!
        board : Board = self.game.reverseCanonicalForm(canonical_board)

        # select a cell
        # paint selection
        # select other which if in range
        # validates the selection
        # repaint for normal output
        # return
        clock = pg.time.Clock()

        start_pos : Position | None = None
        while True:
            clock.tick(self.FPS)

            for event in pg.event.get():
                if (event.type == pg.QUIT    or
                    event.type == pg.KEYDOWN and event.key in [pg.K_ESCAPE, pg.K_q]):
                    print('Display terminating the program.')
                    print(f'Event={event}')
                    pg.quit()
                    exit()

                elif event.type == pg.MOUSEBUTTONDOWN and event.button == 3: # right click
                    valid_vector = self.game.getValidMoves(board, self.game.turn)

                    if valid_vector[-1] == 0:
                        # passing is not valid (there must be available moves)
                        # so do nothing
                        print('AtaxxDisplay.human_play :: Moves available, cannot pass')
                        continue

                    # Pass turn, notify by flashing the screen black
                    self.display_surf.fill((0, 0, 0))
                    pg.display.flip()

                    time.sleep(0.2)
                    self.display(board)

                    return self.game.encode_action(None)

                elif event.type == pg.MOUSEBUTTONDOWN and event.button == 1: # left click
                    col = event.pos[0] // self.cell_size # cell_x
                    row = event.pos[1] // self.cell_size # cell_y

                    if board[row, col] == self.game.turn:
                        # reset any destination painting
                        self.display(board)

                        # First click
                        start_pos = (row, col)

                        color : str
                        if self.game.turn == 1: color = 'red'
                        else:                   color = 'blue'

                        # paint selection
                        self.display_surf.blit( self.assets[f'{color}_selected'], (start_pos[1]*self.cell_size, start_pos[0]*self.cell_size) )

                        # paint possible destinations
                        for pos in self.game.iter_neighborhood(start_pos, clone_neighborhood):

                            if board[pos] == 0:
                                self.display_surf.blit( self.assets[f'{color}_clone'], (pos[1]*self.cell_size, pos[0]*self.cell_size) )

                        for pos in self.game.iter_neighborhood(start_pos, jump_neighborhood):
                            if board[pos] == 0:
                                self.display_surf.blit( self.assets[f'{color}_jump'], (pos[1]*self.cell_size, pos[0]*self.cell_size) )


                        pg.display.flip()

                    elif start_pos is not None:
                        # Second Click
                        dest = (row, col)

                        # valid move iff destination is empty and in one of the neighborhoods
                        # either way we return the action or reset the selection as an invalid one

                        # reset back destination painting
                        self.display(board)

                        # empty test is faster if placed outside but less clear,
                        # and speed in human play is not a problem
                        for pos in self.game.iter_neighborhood(start_pos, clone_neighborhood):
                            if pos == dest and board[dest] == 0:
                                # valid destination
                                return self.game.encode_action(dest)

                        for pos in self.game.iter_neighborhood(start_pos, jump_neighborhood):
                            if pos == dest and board[dest] == 0:
                                # valid destination
                                offset = (dest[0] - start_pos[0], dest[1] - start_pos[1])
                                return self.game.encode_action((start_pos, offset))

                        # not valid -> resetting
                        start_pos = None

########################################################################
## Helpers
def load_image(filename     : str,
               cell_size    : int,
               transparency : bool = False,
               ) -> pg.Surface:

    path = os.path.join(main_dir, '0.assets', filename)

    image : pg.Surface \
        = pg.image.load(path).convert()

    image = pg.transform.smoothscale(image, (cell_size, cell_size))

    if transparency:
        center = image.get_rect().center
        colorkey = image.get_at( center )
        image.set_colorkey(colorkey)

    return image
