import numpy as np

from ataxx.ataxxGame    import AtaxxGame, Board

class RandomPlayer:
    game : AtaxxGame

    def __init__(self, game : AtaxxGame):
        self.game = game

    def play(self, canonicalBoard : Board):
        '''
        Makes a random choice from the possible moves, for that it
        accumulates them first from the action space
        '''

        valid_actions = []
        valids = self.game.getValidMoves(canonicalBoard, 1)
        for action, valid in enumerate(valids):
            if valid == 1:
                valid_actions.append(action)

        return np.random.choice(valid_actions)

class GreedyPlayer:
    game : AtaxxGame

    def __init__(self, game : AtaxxGame):
        self.game = game

    def getRelativeScore(self, canonicalBoard : Board):
        '''
        Counts the pieces difference from canonical player and other player

        Player is hardcoded as 1 since it's the canonical player
        '''
        diff = 0

        for row in range(self.game.n):
            for col in range(self.game.n):

                if   canonicalBoard[row, col] ==  1: diff += 1
                elif canonicalBoard[row, col] == -1: diff -= 1

        return diff

    def play(self, canonicalBoard : Board):
        '''
        Select the play which increases the most the advantage the current player has.

        On ties, breaks them randomly
        '''
        valids = self.game.getValidMoves(canonicalBoard, 1)

        # Since we want all the possible maximums and not one we accumulate them in a list
        greedy_actions = []
        greedy_score   = -np.inf

        for action, valid in enumerate(valids):
            if valid == 0: continue # invalid action

            nextBoard, _ = self.game.getNextState(canonicalBoard, 1, action)
            score        = self.getRelativeScore(nextBoard)

            if score > greedy_score:
                greedy_score   = score
                greedy_actions = [action]

            elif score == greedy_score:
                greedy_actions.append(action)

        return np.random.choice(greedy_actions)
